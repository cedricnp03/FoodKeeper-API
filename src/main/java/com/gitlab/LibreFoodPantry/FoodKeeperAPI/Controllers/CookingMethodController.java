package com.gitlab.LibreFoodPantry.FoodKeeperAPI.Controllers;

import java.util.Collection;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.LibreFoodPantry.FoodKeeperAPI.Database;
import com.gitlab.LibreFoodPantry.FoodKeeperAPI.Models.CookingMethod;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/cookingMethods")
@Api()
public class CookingMethodController {
	private Map<Integer, CookingMethod> cookingMethodDb = Database.getCookingMethodDb();
	
	@GetMapping(value = "")
	@ApiOperation("Returns a list of all cooking methods")
	public ResponseEntity<Collection<CookingMethod>> getAllCookingMethods() {
		if (cookingMethodDb != null) {
			return new ResponseEntity<>(cookingMethodDb.values(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value = "/{id}")
	@ApiOperation("Returns a specific cooking method based on its unique identifier")
	public ResponseEntity<CookingMethod> getCookingMethodById(@PathVariable("id") Integer id) {
		if (cookingMethodDb.containsKey(id)) {
			return new ResponseEntity<>(cookingMethodDb.get(id), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
}
